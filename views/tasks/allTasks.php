<!doctype html>
<html lang="en">
<?php
    include_once (ROOT. '/views/layouts/header.php');
?>
<body>
<?php
    include_once (ROOT. '/views/layouts/navbar.php');
?>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-xs-12 col-mg-12">
            <p class="text-center lead high"> Высокий приоритет </p>
            <div class="list-group">
                <?php foreach ($highPriorityTasks as $task) { ?>
                    <a href="/tasks/<?php echo $task['id']?>" class="list-group-item list-group-item-action flex-column align-items-start"
                       style="width: 100%">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><?php echo $task['task'] ?></h5>
                            <small><?php echo $task['datetime'] ?></small>
                        </div>
                        <p class="mb-1 text-justify"><?php echo $task['description'] ?></p>
                        <div class="d-flex justify-content-between">
                            <small class="mb-1 <?php echo $task['priority_name'] ?>">
                                <?php echo $task['priority_name'] ?>
                            </small>
                            <small class="mb-1">by <?php echo $task['username'] ?></small>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-4 col-xs-12 col-mg-12">
            <p class="text-center lead middle"> Средний приоритет </p>
            <div class="list-group">
                <?php foreach ($middlePriorityTasks as $task) { ?>
                    <a href="/tasks/<?php echo $task['id']?>" class="list-group-item list-group-item-action flex-column align-items-start"
                       style="width: 100%">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><?php echo $task['task'] ?></h5>
                            <small><?php echo $task['datetime'] ?></small>
                        </div>
                        <p class="mb-1 text-justify"><?php echo $task['description'] ?></p>
                        <div class="d-flex justify-content-between">
                            <small class="mb-1 <?php echo $task['priority_name'] ?>">
                                <?php echo $task['priority_name'] ?>
                            </small>
                            <small class="mb-1">by <?php echo $task['username'] ?></small>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-4 col-xs-12 col-mg-12">
           <p class="text-center lead low"> Низкий приоритет </p>
            <div class="list-group">
                <?php foreach ($lowPriorityTasks as $task) { ?>
                    <a href="/tasks/<?php echo $task['id']?>" class="list-group-item list-group-item-action flex-column align-items-start"
                       style="width: 100%">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><?php echo $task['task'] ?></h5>
                            <small><?php echo $task['datetime'] ?></small>
                        </div>
                        <p class="mb-1 text-justify"><?php echo $task['description'] ?></p>
                        <div class="d-flex justify-content-between">
                            <small class="mb-1 <?php echo $task['priority_name'] ?>">
                                <?php echo $task['priority_name'] ?>
                            </small>
                            <small class="mb-1">by <?php echo $task['username'] ?></small>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</body>
