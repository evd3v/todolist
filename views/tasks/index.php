<!doctype html>
<html lang="en">
<?php
    include_once (ROOT. '/views/layouts/header.php');
?>
<body>
<?php
    include_once (ROOT. '/views/layouts/navbar.php');
?>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-xs-12 col-mg-12">
            <p class="text-center lead"> Добавить задачу </p>
            <form action=../../index.php method="post">
                <div class="form-group">
                    <label for="nameOfTheTask">Задача</label>
                    <input type="text" class="form-control" id="nameOfTheAuthor" placeholder="Название задачи"
                           name="nameOfTheTask" required>
                </div>
                <div class="form-group">
                    <label for="descriptionOfTheTask">Описание</label>
                    <textarea class="form-control" id="descriptionOfTheTask" rows="3"
                              placeholder="Введите сюда что-нибудь" name="descriptionOfTheTask" required></textarea>
                </div>
                <div class="form-group">
                    <label for="priorityOfTheTask">Приоритет задачи</label>
                    <select class="form-control" id="priorityOfTheTask" name="priorityOfTheTask" required>
                        <option disabled selected value> -- select an option --</option>
                        <option>High</option>
                        <option>Middle</option>
                        <option>Low</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nameOfTheAuthor">Кто Вы?</label>
                    <input type="text" class="form-control" id="nameOfTheAuthor" placeholder="Ваше имя"
                           name="nameOfTheAuthor" required>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary justify-content-end">Добавить</button>
                </div>
            </form>
        </div>
        <div class="col-lg-4 col-xs-12 col-mg-12">
            <p class="text-center lead"><a href="/tasks/all"> Текущие задачи </a></p>
            <div class="list-group">
                <?php
                if(!$tasksList) {
                echo 'Задач пока нет...';
                }
                ?>
                <?php foreach ($tasksList as $task) { ?>
                    <a href="/tasks/<?php echo $task['id'] ?>" class="list-group-item list-group-item-action flex-column align-items-start"
                       style="width: 100%">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><?php echo $task['task'] ?></h5>
                            <small><?php echo $task['datetime'] ?></small>
                        </div>
                        <p class="mb-1 text-justify"><?php echo $task['description'] ?></p>
                        <div class="d-flex justify-content-between">
                            <small class="mb-1 <?php echo $task['priority_name'] ?>">
                                <?php echo $task['priority_name'] ?>
                            </small>
                            <small class="mb-1">by <?php echo $task['username'] ?></small>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 col-mg-12">
            <p class="text-center lead"><a href="/tasks/archive"> Архив задач </a></p>
            <div class="list-group">
                <?php
                if(!$archiveList) {
                    echo 'Пока архив пуст...';
                }
                    ?>
                <?php foreach ($archiveList as $archiveTask) { ?>
                    <a href="/tasks/<?php echo $archiveTask['id'] ?>" class="list-group-item list-group-item-action flex-column align-items-start"
                       style="width: 100%">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><?php echo $archiveTask['task'] ?></h5>
                            <small><?php echo $archiveTask['datetime'] ?></small>
                        </div>
                        <p class="mb-1 text-justify"><?php echo $archiveTask['description'] ?></p>
                        <div class="d-flex justify-content-between">
                            <small class="mb-1 <?php echo $archiveTask['priority_name'] ?>">
                                <?php echo $archiveTask['priority_name'] ?>
                            </small>
                            <small class="mb-1">by <?php echo $archiveTask['username'] ?></small>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>

