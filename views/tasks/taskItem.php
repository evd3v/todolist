<!doctype html>
<html lang="en">
<?php
    include_once(ROOT . '/views/layouts/header.php');
?>
<body>
<?php
    include_once(ROOT . '/views/layouts/navbar.php');
?>
<div class="row">
    <div class="col-md-6 offset-sm-3">
        <p class="text-center lead">
            <?php
            echo $taskItem['task'];
            if ($taskItem['archive'] == 1) {
                echo ' (в архиве)';
            } ?>
        </p>
        <div class="list-group">
            <a href="/tasks/<?php echo $taskItem['id'] ?>"
               class="list-group-item list-group-item-action flex-column align-items-start"
               style="width: 100%">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1"><?php echo $taskItem['task'] ?></h5>
                    <small><?php echo $taskItem['datetime'] ?></small>
                </div>
                <p class="mb-1 text-justify"><?php echo $taskItem['description'] ?></p>
                <div class="d-flex justify-content-between">
                    <small class="mb-1 <?php echo $taskItem['priority_name'] ?>">
                        <?php echo $taskItem['priority_name'] ?>
                    </small>
                    <small class="mb-1">by <?php echo $taskItem['username'] ?></small>
                </div>
            </a>
        </div>
        <form action="/delete/<?php echo $taskItem['id'] ?>" method="post">
            <div class="row">
                <div class="col">
                    <select class="form-control" id="deleteTask" name="deleteTask" required>
                        <option disabled selected value> -- select an option --</option>
                        <?php
                        echo $taskItem['task'];
                        if ($taskItem['archive'] == 1) {
                            ?>
                            <option>Удалить</option>
                            <option>В работу (из архива)</option>
                            <?php
                        } else {
                            ?>
                            <option>Выполнена (в архив)</option>
                            <option>Удалить</option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary justify-content-end">Выполнить</button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>