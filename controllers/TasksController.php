<?php

include_once ROOT. '/models/Tasks.php';

class TasksController
{
    public function actionIndex()
    {
        if($_POST){
            $task = $_POST['nameOfTheTask'];
            $description = $_POST['descriptionOfTheTask'];
            $priority = $_POST['priorityOfTheTask'];
            $name = $_POST['nameOfTheAuthor'];
            Tasks::addTask($task, $description, $priority, $name);
        }

        if (!empty($_POST)) {
            header('Location: /index.php');
        }

        $tasksList = array();
        $tasksList = Tasks::getTasksList();

        $archiveList = array();
        $archiveList = Tasks::getArchiveTasks();

        require_once(ROOT.'/views/tasks/index.php');
        return true;
    }

    public function actionAllTasks()
    {
        $highPriorityTasks = array();
        $highPriorityTasks = Tasks::getHighPriorityTasks();

        $middlePriorityTasks = array();
        $middlePriorityTasks = Tasks::getMiddlePriorityTasks();

        $lowPriorityTasks = array();
        $lowPriorityTasks = Tasks::getLowPriorityTasks();

        require_once(ROOT.'/views/tasks/allTasks.php');

        return true;
    }

    public function actionViewTask($numOfTask)
    {


        if($numOfTask) {
            $taskItem = Tasks::getTaskById($numOfTask);


            require_once(ROOT.'/views/tasks/taskItem.php');
        }

        return true;
    }

    public function actionAllArchiveTasks()
    {
        $highPriorityArhiveTasks = array();
        $highPriorityArhiveTasks = Tasks::getHighPriorityArchive();

        $middlePriorityArhiveTasks = array();
        $middlePriorityArhiveTasks = Tasks::getMiddlePriorityArchive();

        $lowPriorityArhiveTasks = array();
        $lowPriorityArhiveTasks = Tasks::getLowPriorityArchive();

        require_once(ROOT.'/views/tasks/archiveTasks.php');
        return true;
    }

    public function actionChangeStatus($numOfTask)
    {
        if($_POST['deleteTask'] == "Выполнена (в архив)"){
            Tasks::archiveTask($numOfTask);
            if (!empty($_POST)) {
                header('Location: /index.php');
            }
            return true;
        }

        if($_POST['deleteTask'] == "Удалить"){
            Tasks::deleteTask($numOfTask);
            if (!empty($_POST)) {
                header('Location: /index.php');
            }
            return true;
        }

        if($_POST['deleteTask'] == "В работу (из архива)"){
            Tasks::fromArchiveToTask($numOfTask);
            if (!empty($_POST)) {
                header('Location: /index.php');
            }
            return true;
        }

        return true;
    }

   }