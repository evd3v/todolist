<?php

class Tasks
{

    public static function getTaskById($id)
    {

        $db = Db::getConnection();
        $resultQuery = mysqli_query($db, "SELECT * FROM `tasks` WHERE `id`=$id");

        $result = mysqli_fetch_assoc($resultQuery);

        switch ($result['priority']) {
            case(1):
                $result['priority_name'] = 'low';
                break;
            case(2):
                $result['priority_name'] = 'middle';
                break;
            case(3):
                $result['priority_name'] = 'high';
                break;
            default:
                break;
        }

        return $result;
    }

    public static function getTasksList()
    {

        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
         . '`task`, `description`, `datetime`, `username`, `priority`'
         . 'FROM tasks WHERE archive = 0 ORDER BY id LIMIT 3');

        return self::addIntoArray($tasksList,$result);

    }

    public static function addTask($task, $description, $priority, $name)
    {

        $db = Db::getConnection();

        switch ($priority) {
            case ('Low'):
                $priority = 1;
                break;
            case ('Middle'):
                $priority = 2;
                break;
            case ('High'):
                $priority = 3;
                break;
            default:
                break;
        }

        $addQuery = "INSERT INTO tasks(task,description,username,priority) VALUES('$task','$description','$name',$priority);";
        $result = mysqli_query($db, $addQuery);
        return true;
    }


    public static function getArchiveTasks()
    {

        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
            . '`task`, `description`, `datetime`, `username`, `priority`'
            . 'FROM tasks WHERE archive=1 ORDER BY id LIMIT 3');

        return self::addIntoArray($tasksList,$result);
    }

    public static function getHighPriorityTasks()
    {
        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
            . '`task`, `description`, `datetime`, `username`, `priority`'
            . 'FROM tasks WHERE priority=3 AND archive=0 ORDER BY id');

        return self::addIntoArray($tasksList,$result);
    }

    public static function getMiddlePriorityTasks()
    {
        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
            . '`task`, `description`, `datetime`, `username`, `priority`'
            . 'FROM tasks WHERE priority=2 AND archive=0 ORDER BY id');

        return self::addIntoArray($tasksList,$result);
    }

    public static function getLowPriorityTasks()
    {
        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
            . '`task`, `description`, `datetime`, `username`, `priority`'
            . 'FROM tasks WHERE priority=1 AND archive=0 ORDER BY id');

        return self::addIntoArray($tasksList,$result);
    }

    public static function getLowPriorityArchive()
    {
        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
            . '`task`, `description`, `datetime`, `username`, `priority`'
            . 'FROM tasks WHERE priority=1 AND archive=1 ORDER BY id');

        return self::addIntoArray($tasksList,$result);
    }

    public static function getMiddlePriorityArchive()
    {
        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
            . '`task`, `description`, `datetime`, `username`, `priority`'
            . 'FROM tasks WHERE priority=2 AND archive=1 ORDER BY id');

        return self::addIntoArray($tasksList,$result);
    }

    public static function getHighPriorityArchive()
    {
        $db = Db::getConnection();

        $tasksList = array();

        $result = mysqli_query($db, 'SELECT `id`,'
            . '`task`, `description`, `datetime`, `username`, `priority`'
            . 'FROM tasks WHERE priority=3 AND archive=1 ORDER BY id');

        return self::addIntoArray($tasksList,$result);
    }


    public static function deleteTask($numOfTask)
    {
        $db = Db::getConnection();
        $request = "DELETE FROM tasks WHERE id=$numOfTask";
        $result = mysqli_query($db, $request);
        return true;
    }

    public static function archiveTask($numOfTask)
    {
        $db = Db::getConnection();
        $request = "UPDATE tasks SET archive = 1 WHERE id =$numOfTask;";
        $result = mysqli_query($db, $request);
        return true;
    }

    public static function fromArchiveToTask($numOfTask)
    {
        $db = Db::getConnection();
        $request = "UPDATE tasks SET archive = 0 WHERE id =$numOfTask;";
        $result = mysqli_query($db, $request);
        return true;
    }

    public static function addIntoArray($tasksList, $result)
    {
        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $tasksList[$i]['id'] = $row['id'];
            $tasksList[$i]['task'] = $row['task'];

            if(strlen($row['description']) >= 150) {
                $tasksList[$i]['description'] = substr($row['description'],0,150) . '...';
            } else {
                $tasksList[$i]['description'] = $row['description'];
            }

            $tasksList[$i]['datetime'] = $row['datetime'];
            $tasksList[$i]['username'] = $row['username'];
            $tasksList[$i]['priority'] = $row['priority'];
            switch ($tasksList[$i]['priority']) {
                case(1):
                    $tasksList[$i]['priority_name'] = 'low';
                    break;
                case(2):
                    $tasksList[$i]['priority_name'] = 'middle';
                    break;
                case(3):
                    $tasksList[$i]['priority_name'] = 'high';
                    break;
                default:
                    break;
            }
            $i++;
        }

        return $tasksList;
    }
}



