<?php


class Router
{

    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath); // Подключаем маршруты
    }

    private function getURL()
    {
        if (!empty($_SERVER['REQUEST_URI'])) { // Если запрос не пустой
            return trim($_SERVER['REQUEST_URI'], '/'); // Если первый символ - '/' - то мы его удаляем
        }
    }


    public function run()
    {
        // Получить строку запроса

        $url = $this->getURL();

        // Проверить наличие такого запроса в routes.php

        foreach ($this->routes as $urlPattern => $path)
        {
            // ищем нужный маршрут
            if(preg_match("~$urlPattern~",$url))
            {
                // Внутренний путь, ищем параметры
                $internalRoute = preg_replace("~$urlPattern~", $path, $url);

                // Если есть совпадение, определяем какой контроллер и action обрабатывают запрос
                $segments = explode('/',$internalRoute);

                $controllerName = ucfirst(array_shift($segments)).'Controller';
                $actionName = 'action'.ucfirst(array_shift($segments));

                $parameters = $segments;

                // Подключить файл класса-контроллера
                $controllerFile = ROOT.'/controllers/'.$controllerName.'.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                $controllerObject = new $controllerName;
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if($result != null) {
                    break;
                }

            }
        }


        // Создать объект, вызвать метод (т.е. action)
    }

}