<?php

return array(
    'tasks/([0-9]+)' => 'tasks/viewTask/$1',
    'tasks/all' => 'tasks/allTasks', // actionAllTasks в TasksController
    'tasks/archive' => 'tasks/allArchiveTasks',
    'tasks' => 'tasks/index', // actionIndex в TasksController
    'index.php' => 'tasks/index',
    'delete/([0-9]+)' => 'tasks/changeStatus/$1',
    '' => 'tasks/index',
);
